#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 tycho
# Copyright (c) 2018 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import urlparse
import re
import lxml.html
from .base import ExceptionMixin, RequestMixin, UtilityMixin, StandardFactory, GetReleaseResultMixin, GetListResultMixin
from .base import Scraper as ScraperBase
from .base import SearchScraper as SearchScraperBase


READABLE_NAME = 'Google Play'
SCRAPER_URL = 'https://play.google.com/store/music/'


class GoogleplayMixin(object):

    def get_headers(self):
        headers = {
            'User-Agent': 'curl/7.52.1',
            'accept-language': 'en-GB,en;q=0.9,de;q=0.7'
        }
        return headers

    def process_initial_data(self, initial_data):
        # get the raw response content and parse it
        return lxml.html.document_fromstring(initial_data)


class Scraper(GoogleplayMixin, GetReleaseResultMixin, ScraperBase, RequestMixin, ExceptionMixin, UtilityMixin):

    string_regex = r'^http(?:s)?://play\.google\.com/store/music/album/(.+)\?id=(.+)'
    _base_url = SCRAPER_URL

    VARIOUS_ARTISTS_ALIASES = ['various artists']

    def __init__(self, id, release_name=''):
        super(Scraper, self).__init__()
        self.id = id

        self._release_name = release_name

        self._info_dict = None

        self._release_artists_equal_track_artists = True

    @staticmethod
    def _get_args_from_match(match):
        return reversed(match.groups())

    def get_url(self):
        return self._base_url + 'album/%s?id=%s' % (self._release_name, self.id)

    def _get_info_dict(self):
        if self._info_dict is None:
            self._info_dict = {}
            info_section = self.data.cssselect('.metadata .details-section-contents')[0]
            info_elements = info_section.cssselect('.meta-info')
            info_title_to_key = {
                'Released': 'released',
                'File type': 'format',
                'Label': 'label',
                'Genres': 'genres',
            }
            for info_element in info_elements:
                title = info_element.cssselect('.title')[0].text_content()
                if title not in info_title_to_key.keys():
                    continue
                key = info_title_to_key[title]
                self._info_dict[key] = info_element.cssselect('.content')[0]

        return self._info_dict

    def add_release_event(self):
        info_dict = self._get_info_dict()
        if 'released' in info_dict:
            release_event = self.result.create_release_event()
            release_event.set_date(info_dict['released'].text_content())
            self.result.append_release_event(release_event)

    def add_release_format(self):
        info_dict = self._get_info_dict()
        if 'format' in info_dict:
            self.result.set_format(self.remove_whitespace(info_dict['format'].text_content()))

    def add_label_ids(self):
        info_dict = self._get_info_dict()
        label_content = self.remove_whitespace(info_dict['label'].text_content())
        match = re.search(r'\xa9 \d{4} (.*)', label_content)
        if match:
            labels = match.group(1)
            for label in labels.split('/'):
                label_name = self.remove_whitespace(label)
                label_id = self.result.create_label_id()
                label_id.set_label(label_name)
                self.result.append_label_id(label_id)

    def add_genres(self):
        info_dict = self._get_info_dict()
        if 'genres' in info_dict:
            genre_as = info_dict['genres'].cssselect('a.category')
            for genre_a in genre_as:
                self.result.append_genre(genre_a.text_content())

    def add_release_title(self):
        title_h1 = self.data.cssselect('.document-title *[itemprop="name"]')
        if len(title_h1) != 1:
            self.raise_exception(u'could not find album title h1')
        title_h1 = title_h1[0]
        title = self.remove_whitespace(title_h1.text_content())
        if title:
            self.result.set_title(title)

    def add_release_artists(self):
        artist_a = self.data.cssselect('div[itemprop="byArtist"] .primary')
        if not artist_a:
            self.raise_exception(u'could not find artist a')
        artist_name = self.remove_whitespace(artist_a[0].text_content())
        artist = self.result.create_artist()
        if artist_name.lower() in self.VARIOUS_ARTISTS_ALIASES:
            artist.set_various(True)
        else:
            artist.set_name(artist_name)
        artist.append_type(self.result.ArtistTypes.MAIN)
        self.result.append_release_artist(artist)

    def get_disc_containers(self):
        tracklist_section = self.data.cssselect('.tracks')
        if len(tracklist_section) != 1:
            self.raise_exception(u'could not find tracklist table')
        disc_tables = tracklist_section[0].cssselect('table.track-list')
        disc_containers = {}
        for index, disc_table in enumerate(disc_tables):
            disc = []
            rows = disc_table.cssselect('tr[itemprop="tracks"]')
            for row in rows:
                disc.append(row)
            disc_containers[index + 1] = disc

        return disc_containers

    def get_track_number(self, track_container):
        track_number_div = track_container.cssselect('.track-number')
        if not track_number_div:
            self.raise_exception(u'could not extract track number')
        track_number = track_number_div[0].text_content()
        return track_number

    def get_track_title(self, track_container):
        track_title_div = track_container.cssselect('.title')[0]
        track_title = self.remove_whitespace(track_title_div.text_content())
        return track_title

    def get_track_artists(self, track_container):
        artist_div = track_container.cssselect('.artist')

        if not artist_div:
            return []

        artist_name = artist_div[0].text_content()
        track_artist = self.result.create_artist()
        track_artist.set_name(artist_name)
        track_artist.append_type(self.result.ArtistTypes.MAIN)
        return [track_artist]

    def get_track_length(self, track_container):
        track_length_div = track_container.cssselect('.duration')[0]
        track_length = self.remove_whitespace(track_length_div.text_content())
        if track_length:
            return self.seconds_from_string(track_length)
        return None


class SearchScraper(GoogleplayMixin, GetListResultMixin, SearchScraperBase, RequestMixin, ExceptionMixin, UtilityMixin):

    url = 'https://play.google.com/store/search?q=%s&c=music'

    def get_url(self):
        return self.url % self.search_term

    def get_release_containers(self):
        clusters = self.data.cssselect('.cluster')
        album_cluster = None
        for cluster in clusters:
            title = cluster.cssselect('.title-link')[0].text_content()
            if title == 'Albums':
                album_cluster = cluster
                break

        try:
            containers = album_cluster.cssselect('.card-content .details')
            return containers
        except AttributeError:
            return []

    def _get_title_a(self, release_container):
        title_a = release_container.cssselect('a.title')
        return title_a[0]

    def get_release_name(self, release_container):
        artist_div = release_container.cssselect('a.subtitle')

        if not artist_div:
            self.raise_exception(u'could not extract artists')
        artist_name = artist_div[0].text_content()

        title_a = self._get_title_a(release_container)
        title = title_a.text_content()
        return artist_name + u' \u2013 ' + title

    def get_release_url(self, release_container):
        title_a = self._get_title_a(release_container)
        release_url = urlparse.urljoin(SCRAPER_URL, title_a.attrib['href'])
        m = re.match(Scraper.string_regex, release_url)
        if not m:
            release_url = None
        return release_url


class ScraperFactory(StandardFactory):

    scraper_classes = (Scraper, )
    search_scraper = SearchScraper
