#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 tycho
# Copyright (c) 2018 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import lxml.html
import re
import itertools
from .base import Scraper, ExceptionMixin, RequestMixin, UtilityMixin, StandardFactory, GetReleaseResultMixin, GetListResultMixin, StatusCodeError
from .base import SearchScraper as SearchScraperBase
from ..result import NotFoundResult


READABLE_NAME = 'AllMusic'
SCRAPER_URL = 'https://www.allmusic.com/'
NOTES = ''

UNKNOWN_ARTIST = 'Unknown Artist'
MISSING_TITLE = '[missing title information]'


class GenericResultScraper(GetReleaseResultMixin, Scraper, RequestMixin, ExceptionMixin, UtilityMixin):

    _base_url = SCRAPER_URL

    VARIOUS_ARTISTS_ALIASES = ['various artists']

    def __init__(self, id, release_name=''):
        super(GenericResultScraper, self).__init__()
        self.id = id

        self._release_name = release_name

        self._info_dict = None

        self._release_artists_equal_track_artists = True

    @staticmethod
    def _get_args_from_match(match):
        return int(match.group(2)), match.group(1)

    def get_request_kwargs(self):
        return {'allow_redirects': False}

    def handle_initial_data_exception(self, exception):
        if isinstance(exception, StatusCodeError) and str(exception) == '301':
            self.result = self.instantiate_result(NotFoundResult)
        else:
            super(GenericResultScraper, self).handle_initial_data_exception(exception)

    def process_initial_data(self, initial_data):
        #get the raw response content and parse it
        return lxml.html.document_fromstring(initial_data)

    def _get_info_dict(self):
        if self._info_dict is None:
            self._info_dict = {}
            info_section = self.data.cssselect('section.basic-info')[0]
            info_key_to_selector = {
                'released': 'div.release-date',
                'format': 'div.format',
                'cat_id': 'div.catalog-number',
                'label': 'div.label',
                'genres': 'div.genre',
                'styles': 'div.styles'
            }
            for key, selector in info_key_to_selector.iteritems():
                info_element = info_section.cssselect(selector)
                if len(info_element) != 1:
                    continue
                info_value = info_element[0].cssselect('> div, > span')[0]
                self._info_dict[key] = info_value

            for key in ['released', 'format', 'cat_id', 'genres']:
                if key in self._info_dict:
                    self._info_dict[key] = self.remove_whitespace(self._info_dict[key].text_content())

        return self._info_dict

    def _check_if_release_artists_equals_track_artists(self):
        release_artists = sorted(self.result.get_release_artists())
        discs = self.get_disc_containers()
        for track_container in itertools.chain(*discs.values()):
            track_main_artists = sorted(self._get_track_main_artists(track_container))
            self._release_artists_equal_track_artists = release_artists == track_main_artists
            if not self._release_artists_equal_track_artists:
                break

    def add_release_event(self):
        info_dict = self._get_info_dict()
        if 'released' in info_dict:
            release_event = self.result.create_release_event()
            release_event.set_date(info_dict['released'])
            self.result.append_release_event(release_event)

    def add_release_format(self):
        info_dict = self._get_info_dict()
        if 'format' in info_dict:
            self.result.set_format(info_dict['format'])

    def add_label_ids(self):
        info_dict = self._get_info_dict()
        labels = []

        if 'label' in info_dict:
            label_as = info_dict['label'].cssselect('a')
            for label_a in label_as:
                label_id = self.result.create_label_id()
                label_id.set_label(self.remove_whitespace(label_a.text_content()))
                labels.append(label_id)

        if labels and 'cat_id' in info_dict:
            cat_id = info_dict['cat_id']
            for label in labels:
                label.append_catalogue_nr(cat_id)

        for label in labels:
            self.result.append_label_id(label)

    def add_genres(self):
        info_dict = self._get_info_dict()
        if 'genres' in info_dict:
            self.result.append_genre(info_dict['genres'])

    def add_styles(self):
        info_dict = self._get_info_dict()
        if 'styles' in info_dict:
            for style_a in info_dict['styles'].cssselect('a'):
                self.result.append_style(self.remove_whitespace(style_a.text_content()))

    def add_release_title(self):
        title_h1 = self.data.cssselect('div#cmn_wrap h1.%s-title' % self.scraper_type)
        if len(title_h1) != 1:
            self.raise_exception(u'could not find album title h1')
        title_h1 = title_h1[0]
        title = self.remove_whitespace(title_h1.text_content())
        if title:
            self.result.set_title(title)

    def add_release_artists(self):
        artist_h2 = self.data.cssselect('div#cmn_wrap h2.%s-artist' % self.scraper_type)
        if not artist_h2:
            self.raise_exception(u'could not find artist h2')
        artist_h2 = artist_h2[0]

        artist_as = artist_h2.cssselect('a')
        for artist_a in artist_as:
            artist_name = self.remove_whitespace(artist_a.text_content())
            artist = self.result.create_artist()
            artist.set_name(artist_name)
            artist.append_type(self.result.ArtistTypes.MAIN)
            self.result.append_release_artist(artist)
        if not artist_as:
            artist_name = self.remove_whitespace(artist_h2.text_content())
            artist = self.result.create_artist()
            if artist_name.lower() in self.VARIOUS_ARTISTS_ALIASES:
                artist.set_various(True)
            else:
                if not artist_name:
                    artist_name = UNKNOWN_ARTIST
                artist.set_name(artist_name)
            artist.append_type(self.result.ArtistTypes.MAIN)
            self.result.append_release_artist(artist)

        self._check_if_release_artists_equals_track_artists()

    def get_disc_containers(self):
        tracklist_section = self.data.cssselect('div#cmn_wrap section.track-listing')
        if len(tracklist_section) != 1:
            return self.empty_disc_containers()
        tracklist_section = tracklist_section[0]
        disc_divs = tracklist_section.cssselect('div.disc')
        disc_containers = {}
        disc_number = 1
        for disc_div in disc_divs:
            header = disc_div.cssselect('div.headline h3')[0].text_content()
            m = re.search('(?:Track Listing - Disc )(\d+)', self.remove_whitespace(header))
            if m:
                disc_number = int(m.group(1))

            if disc_number not in disc_containers:
                disc_containers[disc_number] = []

            rows = disc_div.cssselect('tbody tr.track')
            for row in rows:
                disc_containers[disc_number].append(row)

        return disc_containers

    def get_track_number(self, track_container):
        track_number_td = track_container.cssselect('td.tracknum')[0]
        m = re.search('(\d+)(?:\.)?', track_number_td.text_content())
        if m:
            track_number_without_zeros = m.group(1).lstrip('0')
            if track_number_without_zeros:
                track_number = track_number_without_zeros
            else:
                track_number = '0'
        else:
            self.raise_exception(u'could not extract track number')
        return track_number

    def get_track_title(self, track_container):
        track_title_composer_td = track_container.cssselect('td.title-composer')
        track_title_div = track_title_composer_td[0].cssselect('div.title')
        track_title = self.remove_whitespace(track_title_div[0].text_content())
        if not track_title:
            return MISSING_TITLE
        return track_title

    def _get_track_main_artists(self, track_container):
        performer_td = track_container.cssselect('td.performer')[0]
        artists = []

        primary_artist_as = performer_td.cssselect('div.primary a')
        if not primary_artist_as:
            return []
        for primary_artist_a in primary_artist_as:
            artist_name = primary_artist_a.text_content()
            track_artist = self.result.create_artist()
            track_artist.set_name(artist_name)
            track_artist.append_type(self.result.ArtistTypes.MAIN)
            artists.append(track_artist)

        return artists

    def get_track_artists(self, track_container):
        performer_td = track_container.cssselect('td.performer')[0]
        artists = []

        if not self._release_artists_equal_track_artists:
            artists = self._get_track_main_artists(track_container)

        featuring_artists_a = performer_td.cssselect('div.featuring a')
        for featuring_artist_a in featuring_artists_a:
            track_feat = self.result.create_artist()
            track_feat.set_name(featuring_artist_a.text_content())
            track_feat.append_type(self.result.ArtistTypes.FEATURING)
            artists.append(track_feat)

        return artists

    def get_track_length(self, track_container):
        track_length_td = track_container.cssselect('td.time')[0]
        track_length = self.remove_whitespace(track_length_td.text_content())
        if track_length:
            return self.seconds_from_string(track_length)
        return None


class ReleaseScraper(GenericResultScraper):
    scraper_type = 'release'
    string_regex = '^http(?:s)?://(?:www\.)?allmusic\.com/album/release/(.*?)-mr(\d{10})$'
    _base_url = SCRAPER_URL

    def __init__(self, id, release_name=''):
        super(ReleaseScraper, self).__init__(id, release_name)
        self.album_scraper = None

    def process_initial_data(self, initial_data):
        #get the raw response content and parse it
        return lxml.html.document_fromstring(initial_data)

    def get_url(self):
        return self._base_url + 'album/release/%s-mr%010d' % (self._release_name, self.id)

    def get_album_scraper(self):
        if not self.album_scraper:
            self.album_scraper = AlbumDataScraper.from_string(self.get_album_url())
            self.album_scraper.initialize_data()
        return self.album_scraper

    def get_album_url(self):
        album_a = self.data.cssselect('div#cmn_wrap section.main-album a.album-title')[0]
        return album_a.get('href')

    def empty_disc_containers(self):
        return self.get_album_scraper().get_disc_containers()

    def _get_info_dict(self):
        if self._info_dict is None:
            album_info_dict = self.get_album_scraper()._get_info_dict()
            release_info_dict = super(ReleaseScraper, self)._get_info_dict()
            self._info_dict = album_info_dict
            self._info_dict.update(release_info_dict)
        return self._info_dict


class AlbumDataScraper(GenericResultScraper):

    string_regex = '^http(?:s)?://(?:www\.)?allmusic\.com/album/(.*?)-mw(\d{10})$'
    scraper_type = 'album'

    def empty_disc_containers(self):
        return {}

    def get_url(self):
        return self._base_url + 'album/%s-mw%010d' % (self._release_name, self.id)


class AlbumScraper(GetListResultMixin, Scraper, RequestMixin, ExceptionMixin, UtilityMixin):

    string_regex = '^http(?:s)?://(?:www\.)?allmusic\.com/album/(.*?)-mw(\d{10})(?:/[^/]*)?$'
    _base_url = SCRAPER_URL

    def __init__(self, id, release_name=''):
        super(AlbumScraper, self).__init__()

        self.id = id

        self._release_name = release_name

        self._artists = None

    @staticmethod
    def _get_args_from_match(match):
        return int(match.group(2)), match.group(1)

    def process_initial_data(self, initial_data):
        #get the raw response content and parse it
        return lxml.html.document_fromstring(initial_data)

    def get_url(self):
        return self._base_url + 'album/%s-mw%010d/releases' % (self._release_name, self.id)

    def get_release_containers(self):
        # all releases share the same artist so we only need to get them once
        artists_h2 = self.data.cssselect('div#cmn_wrap h2.album-artist')
        if not artists_h2:
            self.raise_exception(u'could not find artist h2')
        artists_h2 = artists_h2[0]
        self._artists = self.remove_whitespace(artists_h2.text_content())

        # now we get the actual release containers
        containers = self.data.cssselect('div#cmn_wrap section.releases table.fdata tbody tr')
        return containers

    def _get_title_a(self, release_container):
        title_a = release_container.cssselect('td.title a')
        if len(title_a) != 1:
            self.raise_exception(u'could not extract release title')
        return title_a[0]

    def get_release_name(self, release_container):
        title_a = self._get_title_a(release_container)
        title = self.remove_whitespace(title_a.text_content())
        return self._artists + u' \u2013 ' + title

    def get_release_url(self, release_container):
        title_a = self._get_title_a(release_container)
        release_url = title_a.attrib['href']
        m = re.match(ReleaseScraper.string_regex, release_url)
        if not m:
            release_url = None
        return release_url

    def get_release_info(self, release_container):
        components = []

        date_td = release_container.cssselect('td.year')
        if len(date_td) == 1:
            components.append(self.remove_whitespace(date_td[0].text_content()))

        label_td = release_container.cssselect('td.label-catalog')
        if len(label_td) == 1:
            components.append(self.remove_whitespace(label_td[0].text_content()))

        format_td = release_container.cssselect('td.format')
        if len(format_td) == 1:
            components.append(self.remove_whitespace(format_td[0].text_content()))

        return u' | '.join(filter(lambda x: x, components))


class SearchScraper(GetListResultMixin, SearchScraperBase, RequestMixin, ExceptionMixin, UtilityMixin):

    url = 'https://www.allmusic.com/search/albums/'

    def get_url(self):
        return self.url + self.search_term

    def process_initial_data(self, initial_data):
        #get the raw response content and parse it
        return lxml.html.document_fromstring(initial_data)

    def get_release_containers(self):
        containers = self.data.cssselect('div#cmn_wrap ul.search-results li.album')
        return containers

    def _get_title_a(self, release_container):
        title_a = release_container.cssselect('div.title a')
        if len(title_a) != 1:
            self.raise_exception(u'could not extract release title')
        return title_a[0]

    def get_release_name(self, release_container):
        artist_div = release_container.cssselect('div.artist')

        if not artist_div:
            return UNKNOWN_ARTIST
        artist_div = artist_div[0]

        artist_as = artist_div.cssselect('a')

        artists = []
        for artist_a in artist_as:
            artists.append(self.remove_whitespace(artist_a.text_content()))
        if not artist_as:
            artists.append(self.remove_whitespace(artist_div.text_content()))

        title_a = self._get_title_a(release_container)
        title = title_a.text_content()
        return u', '.join(artists) + u' \u2013 ' + title

    def get_release_url(self, release_container):
        title_a = self._get_title_a(release_container)
        release_url = title_a.attrib['href']
        m = re.match(AlbumScraper.string_regex, release_url)
        if not m:
            release_url = None
        return release_url

    def get_release_info(self, release_container):
        components = []

        info_selectors = ['div.year', 'div.genres']

        for selector in info_selectors:
            element = release_container.cssselect(selector)
            if not element:
                continue
            element_content = self.remove_whitespace(element[0].text_content())
            if element_content:
                components.append(element_content)

        if components:
            return u' | '.join(components)

        return None


class ScraperFactory(StandardFactory):

    scraper_classes = [ReleaseScraper, AlbumScraper]
    search_scraper = SearchScraper
