#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from django import template

register = template.Library()


# inspired by: https://blog.joeymasip.com/how-to-add-attributes-to-form-widgets-in-django-templates/
@register.filter(name='with_attrs')
def get_field_widget_with_attrs(field, attr_string):
    attrs = {}
    definitions = attr_string.split(',')

    for definition in definitions:
        components = definition.split(':', 1)
        if len(components) == 1:
            components.insert(0, 'class')
        attrs.update([components])

    return field.as_widget(attrs=attrs)
