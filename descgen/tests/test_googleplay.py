#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 tycho
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from . import TestCase
from ..scraper import googleplay
from ..result import NotFoundResult


class GoogleplayTest(TestCase):

    def test_simple_album(self):
        expected = {
            u'artists': u'Múm',
            u'title': u'Finally We Are No One',
            u'format': u'MP3',
            u'genres': [u'Alternative/Indie'],
            u'url': u'https://play.google.com/store/music/album/M%C3%BAm_Finally_We_Are_No_One?id=Bdwfqucjvuosybdppd5ztoomooa',
            u'labelIds': [{u'catalogueNrs': [], u'label': u'FatCat Records'}],
            u'releaseEvents': u'20 May 2002',
            u'discs': [
                [
                    [u'Sleep/Swim', 50],
                    [u'Green Grass of Tunnel', 291],
                    [u'We Have a Map of the Piano', 319],
                    [u'Don\'t Be Afraid, You Have Just Got Your Eyes Closed', 343],
                    [u'Behind Two Hills,,,,A Swimmingpool', 68],
                    [u'K/Half Noise', 521],
                    [u'Now There\'s That Fear Again', 236],
                    [u'Faraway Swimmingpool', 175],
                    [u'I Can\'t Feel My Hand Any More, It\'s Alright, Sleep Still', 340],
                    [u'Finally We Are No One', 307],
                    [u'The Land Between Solar Systems', 718],
                ]
            ]
        }

        scraper = googleplay.Scraper.from_string(
            u'https://play.google.com/store/music/album/M%C3%BAm_Finally_We_Are_No_One?id=Bdwfqucjvuosybdppd5ztoomooa'
        )
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_multiple_cds(self):
        expected = {
            u'artists': u'Faith No More',
            u'title': u'Angel Dust (Deluxe Edition)',
            u'format': u'MP3',
            u'genres': [u'Alt Metal', u'Metal', u'Alternative/Indie', u'Rock', u"90's Alternative", u'Hard Rock'],
            u'url': u'https://play.google.com/store/music/album/Faith_No_More_Angel_Dust_Deluxe_Edition?id=Bsbdhlang4ndnzwjcmj7jjvrc4y',
            u'labelIds': [{u'catalogueNrs': [], u'label': u'WM UK'}],
            u'releaseEvents': u'8 June 1992',
            u'discs': [
                [
                    [u'Land Of Sunshine', 224],
                    [u'Caffeine', 268],
                    [u'Midlife Crisis', 259],
                    [u'RV', 223],
                    [u'Smaller And Smaller', 311],
                    [u'Everything\'s Ruined', 274],
                    [u'Malpractice', 242],
                    [u'Kindergarten', 271],
                    [u'Be Aggressive', 222],
                    [u'A Small Victory', 297],
                    [u'Crack Hitler', 279],
                    [u'Jizzlobber', 398],
                    [u'Midnight Cowboy', 252],
                    [u'Easy', 186],
                ],
                [
                    [u'Easy (Cooler Version)', 189],
                    [u'Das Schutzenfest (German Version)', 179],
                    [u'As The Worm Turns (Mike Patton Vocal)', 159],
                    [u'Let\'s Lynch the Landlord', 176],
                    [u'Midlife Crisis (The Scream Mix) [Remixed By Matt Walllace]', 232],
                    [u'A Small Victory (R-Evolution 23) [Full Moon] [Mix]', 441],
                    [u'Easy (Live in Munich 9th November 1992)', 191],
                    [u'Be Aggressive (Live in Munich 9th November 1992)', 221],
                    [u'Kindergarten (Live in Munich 9th November 1992)', 256],
                    [u'A Small Victory (Live in Munich 9th November 1992)', 293],
                    [u'Mark Bowen (Live in Munich 9th November 1992)', 195],
                    [u'We Care A Lot (Live in Munich 9th November 1992)', 239],
                    [u'Midlife Crisis (Live in Dekalb, Illinois 20th September 1992)', 214],
                    [u'Land Of Sunshine (Live in Dekalb, Illinois 20th September 1992)', 216],
                    [u'Edge Of The World (Live in St. Louis 18th September 1992)', 303],
                    [u'RV (Live in Dekalb, Illinois 20th September 1992)', 231],
                    [u'The World Is Yours (Outtake from Angel Dust Sessions)', 352],
                ]
            ]
        }

        scraper = googleplay.Scraper.from_string('https://play.google.com/store/music/album/Faith_No_More_Angel_Dust_Deluxe_Edition?id=Bsbdhlang4ndnzwjcmj7jjvrc4y')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_various_artists(self):
        expected = {
            u'artists': [{u'name': None, u'isVarious': True}],
            u'title': u'Quentin Tarantino’s Django Unchained Original Motion Picture Soundtrack',
            u'format': u'MP3',
            u'genres': [u'Soundtracks'],
            u'url': u'https://play.google.com/store/music/album/Various_Artists_Quentin_Tarantino_s_Django_Unchain?id=Buxie25d2a7rqfdm4a3ikdctueu',
            u'labelIds': [
                {u'catalogueNrs': [], u'label': u'Concord'},
                {u'catalogueNrs': [], u'label': u'Loma Vista'},
                {u'catalogueNrs': [], u'label': u'Caroline'}
            ],
            u'releaseEvents': u'1 January 2012',
            u'discs': [
                [
                    [u'James Russo', u'Winged', 8],
                    [u'Luis Bacalov', u'Django', 173],
                    [u'Ennio Morricone', u'The Braying Mule', 153],
                    [u'Christoph Waltz', u'"In The Case Django, After You..."', 38],
                    [u'Luis Bacalov', u'Lo Chiamavano King (His Name Is King)', 117],
                    [u'Anthony Hamilton', u'Freedom', 236],
                    [u'Don Johnson', u'Five-Thousand-Dollar Nigga\'s And Gummy Mouth Bitches (Album Version Explicit)', 56],
                    [u'Luis Bacalov', u'La Corsa (2nd Version)', 138],
                    [u'Don Straud', u'Sneaky Schultz And The Demise Of Sharp', 34],
                    [u'Jim Croce', u'I Got A Name', 195],
                    [u'Riziero Ortolani', u'I Giorni Dell’ira', 185],
                    [u'Rick Ross', u'100 Black Coffins', 223],
                    [u'Jerry Goldsmith', u'Nicaragua (feat. Pat Metheny)', 209],
                    [u'Samuel L. Jackson', u'Hildi\'s Hot Box (Album Version Explicit)', 76],
                    [u'Ennio Morricone', u'Sister Sara\'s Theme', 86],
                    [u'Elisa Toffoli', u'Ancora Qui', 308],
                    [u'James Brown & 2Pac', u'Unchained (The Payback / Untouchable) (Album Version Explicit)', 171],
                    [u'John Legend', u'Who Did That To You?', 228],
                    [u'Brother Dege (AKA Dege Legg)', u'Too Old To Die Young', 223],
                    [u'Samuel L. Jackson', u'Stephen The Poker Player (Album Version Explicit)', 62],
                    [u'Ennio Morricone', u'Un Monumento', 150],
                    [u'Samuel L. Jackson', u'Six Shots Two Guns (Album Version Explicit)', 5],
                    [u'Annibale E I Cantori Moderni', u'TRINITY: TITOLI', 183],
                ]
            ]
        }

        scraper = googleplay.Scraper.from_string('https://play.google.com/store/music/album/Various_Artists_Quentin_Tarantino_s_Django_Unchain?id=Buxie25d2a7rqfdm4a3ikdctueu')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_404(self):
        expected = NotFoundResult()
        expected.set_scraper_name(None)

        scraper = googleplay.Scraper.from_string('https://play.google.com/store/music/album/invalid?id=xxx')
        result = scraper.get_result()

        self.assertEqual(expected, result)

    def test_search_scraper(self):
        scraper = googleplay.SearchScraper('love')
        result = scraper.get_result()

        self.assertGreater(len(result.get_items()), 0)
