#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from . import TestCase
from ..scraper import itunes
from ..result import ReleaseResult, ListResult, NotFoundResult


class ITunesTest(TestCase):

    def test_simple_album(self):
        expected = ReleaseResult()
        expected.set_scraper_name(None)

        release_event = expected.create_release_event()
        release_event.set_date('1985')
        release_event.set_country(None)
        expected.append_release_event(release_event)

        expected.set_format(None)

        expected.set_title('Love')

        artist = expected.create_artist()
        artist.set_name('The Cult')
        artist.set_various(False)
        artist.append_type(expected.ArtistTypes.MAIN)
        expected.append_release_artist(artist)

        expected.append_genre('Rock')
        expected.append_genre('Adult Alternative')
        expected.append_genre('Hard Rock')
        expected.append_genre('Alternative')
        expected.append_genre('Goth Rock')
        expected.append_genre('College Rock')

        expected.set_url('https://music.apple.com/us/album/love/1028833323')

        disc = expected.create_disc()
        disc.set_number(1)
        disc.set_title(None)

        track = disc.create_track()
        track.set_number('1')
        track.set_title('Nirvana')
        track.set_length(326)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('2')
        track.set_title('Big Neon Glitter')
        track.set_length(291)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('3')
        track.set_title('Love')
        track.set_length(329)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('4')
        track.set_title('Brother Wolf, Sister Moon')
        track.set_length(407)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('5')
        track.set_title('Rain')
        track.set_length(236)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('6')
        track.set_title('Phoenix')
        track.set_length(306)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('7')
        track.set_title('Hollow Man')
        track.set_length(285)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('8')
        track.set_title('Revolution')
        track.set_length(326)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('9')
        track.set_title('She Sells Sanctuary')
        track.set_length(263)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('10')
        track.set_title('Black Angel')
        track.set_length(322)
        disc.append_track(track)

        expected.append_disc(disc)

        s = itunes.ReleaseScraper.from_string('https://music.apple.com/us/album/love/1028833323')
        r = s.get_result()

        self.assertEqual(expected, r)

    def test_multiple_cds(self):
        expected = ReleaseResult()
        expected.set_scraper_name(None)

        release_event = expected.create_release_event()
        release_event.set_date(u'2006-01-10')
        release_event.set_country(None)
        expected.append_release_event(release_event)

        expected.set_format(None)

        expected.set_title(u"Think It's A Game Double Disc Set")

        artist = expected.create_artist()
        artist.set_name(u'Todd Bangz')
        artist.set_various(False)
        artist.append_type(expected.ArtistTypes.MAIN)
        expected.append_release_artist(artist)

        expected.append_genre(u'Hip-Hop/Rap')

        expected.set_url('https://music.apple.com/us/album/think-its-a-game-double-disc-set/125452395')

        disc = expected.create_disc()
        disc.set_number(1)
        disc.set_title(None)

        track = disc.create_track()
        track.set_number('1')
        track.set_title(u'Intro')
        track.set_length(79)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('2')
        track.set_title(u'Breathe Ya Last')
        track.set_length(216)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('3')
        track.set_title(u'The Tongue')
        track.set_length(242)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('4')
        track.set_title(u'Da Jump Off')
        track.set_length(290)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('5')
        track.set_title(u'Starz N Stripes')
        track.set_length(292)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('6')
        track.set_title(u'Your Mercy')
        track.set_length(272)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('7')
        track.set_title(u'God IZ Watchin')
        track.set_length(196)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('8')
        track.set_title(u'Not 4 Me')
        track.set_length(267)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('9')
        track.set_title(u'God Got It')
        track.set_length(270)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('10')
        track.set_title(u"Think It's A Game")
        track.set_length(236)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('11')
        track.set_title(u'Cant Get Enough')
        track.set_length(260)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('12')
        track.set_title(u'Roc City Epistle')
        track.set_length(281)
        disc.append_track(track)

        expected.append_disc(disc)

        disc = expected.create_disc()
        disc.set_number(2)
        disc.set_title(None)

        track = disc.create_track()
        track.set_number('1')
        track.set_title(u'Do Dat')
        track.set_length(249)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('2')
        track.set_title(u'Industry Puppets')
        track.set_length(240)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('3')
        track.set_title(u"Time Keep Slippin'")
        track.set_length(205)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('4')
        track.set_title(u'Christ Is')
        track.set_length(238)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('5')
        track.set_title(u'When Will U Learn')
        track.set_length(266)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('6')
        track.set_title(u'Never Again')
        track.set_length(270)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('7')
        track.set_title(u'Soul Winner')
        track.set_length(267)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('8')
        track.set_title(u'Love & Leave')
        track.set_length(283)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('9')
        track.set_title(u'Battle Cry')
        track.set_length(284)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('10')
        track.set_title(u'Victorious')
        track.set_length(240)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('11')
        track.set_title(u'Love Less')
        track.set_length(260)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('12')
        track.set_title(u'I Repent')
        track.set_length(271)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('13')
        track.set_title(u'Another')
        track.set_length(272)
        disc.append_track(track)

        expected.append_disc(disc)

        s = itunes.ReleaseScraper.from_string('https://music.apple.com/us/album/think-its-a-game-double-disc-set/125452395')
        r = s.get_result()

        self.assertEqual(expected, r)

    def test_various_artists(self):
        expected = ReleaseResult()
        expected.set_scraper_name(None)

        release_event = expected.create_release_event()
        release_event.set_date('Oct 28, 2008')
        release_event.set_country(None)
        expected.append_release_event(release_event)

        expected.set_format(None)

        expected.set_title('Twilight (Original Motion Picture Soundtrack)')

        artist = expected.create_artist()
        artist.set_name(None)
        artist.set_various(True)
        artist.append_type(expected.ArtistTypes.MAIN)
        expected.append_release_artist(artist)

        expected.append_genre('Soundtrack')

        expected.set_url('https://music.apple.com/us/album/twilight-original-motion-picture/294342468?ign-mpt=uo%3D4')

        disc = expected.create_disc()
        disc.set_number(1)
        disc.set_title(None)

        track = disc.create_track()
        track.set_number('1')
        track.set_title('Supermassive Black Hole')
        track.set_length(209)
        track_artist = expected.create_artist()
        track_artist.set_name('Muse')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('2')
        track.set_title('Decode')
        track.set_length(261)
        track_artist = expected.create_artist()
        track_artist.set_name('Paramore')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('3')
        track.set_title('Full Moon')
        track.set_length(230)
        track_artist = expected.create_artist()
        track_artist.set_name('The Black Ghosts')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('4')
        track.set_title('Leave Out All the Rest')
        track.set_length(199)
        track_artist = expected.create_artist()
        track_artist.set_name('LINKIN PARK')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('5')
        track.set_title('Spotlight (Twilight Mix)')
        track.set_length(200)
        track_artist = expected.create_artist()
        track_artist.set_name('MuteMath')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('6')
        track.set_title('Go All the Way (Into the Twilight)')
        track.set_length(207)
        track_artist = expected.create_artist()
        track_artist.set_name('Perry Farrell')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('7')
        track.set_title('Tremble for My Beloved')
        track.set_length(233)
        track_artist = expected.create_artist()
        track_artist.set_name('Collective Soul')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('8')
        track.set_title('I Caught Myself')
        track.set_length(235)
        track_artist = expected.create_artist()
        track_artist.set_name('Paramore')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('9')
        track.set_title('Eyes On Fire')
        track.set_length(301)
        track_artist = expected.create_artist()
        track_artist.set_name('Blue Foundation')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('10')
        track.set_title('Never Think')
        track.set_length(269)
        track_artist = expected.create_artist()
        track_artist.set_name('Rob Pattinson')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('11')
        track.set_title('Flightless Bird, American Mouth')
        track.set_length(240)
        track_artist = expected.create_artist()
        track_artist.set_name('Iron & Wine')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('12')
        track.set_title("Bella's Lullaby")
        track.set_length(138)
        track_artist = expected.create_artist()
        track_artist.set_name('Carter Burwell')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('13')
        track.set_title('Let Me Sign (Bonus Track)')
        track.set_length(138)
        track_artist = expected.create_artist()
        track_artist.set_name('Rob Pattinson')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('14')
        track.set_title('La Traviata (Bonus Track)')
        track.set_length(185)
        track_artist = expected.create_artist()
        track_artist.set_name('Royal Philharmonic Orchestra')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('15')
        track.set_title('Clair de Lune (Bonus Track)')
        track.set_length(358)
        track_artist = expected.create_artist()
        track_artist.set_name('The APM Orchestra')
        track_artist.set_various(False)
        track_artist.append_type(expected.ArtistTypes.MAIN)
        track.append_artist(track_artist)
        disc.append_track(track)

        expected.append_disc(disc)

        s = itunes.ReleaseScraper.from_string('https://music.apple.com/us/album/twilight-original-motion-picture/294342468?ign-mpt=uo%3D4')
        r = s.get_result()

        self.assertEqual(expected, r)

    def test_non_us_store(self):
        expected = ReleaseResult()
        expected.set_scraper_name(None)

        release_event = expected.create_release_event()
        release_event.set_date('1985')
        release_event.set_country(None)
        expected.append_release_event(release_event)

        expected.set_format(None)

        expected.set_title('Love')

        artist = expected.create_artist()
        artist.set_name('The Cult')
        artist.set_various(False)
        artist.append_type(expected.ArtistTypes.MAIN)
        expected.append_release_artist(artist)

        expected.append_genre('Rock')
        expected.append_genre('Musique')
        expected.append_genre('Alternative adulte')
        expected.append_genre('Hard rock')
        expected.append_genre('Alternative')
        expected.append_genre('Rock gothique')
        expected.append_genre('College rock')

        expected.set_url('http://music.apple.com/fr/love/1028833323')

        disc = expected.create_disc()
        disc.set_number(1)
        disc.set_title(None)

        track = disc.create_track()
        track.set_number('1')
        track.set_title('Nirvana')
        track.set_length(326)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('2')
        track.set_title('Big Neon Glitter')
        track.set_length(291)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('3')
        track.set_title('Love')
        track.set_length(329)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('4')
        track.set_title('Brother Wolf, Sister Moon')
        track.set_length(407)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('5')
        track.set_title('Rain')
        track.set_length(236)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('6')
        track.set_title('Phoenix')
        track.set_length(306)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('7')
        track.set_title('Hollow Man')
        track.set_length(285)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('8')
        track.set_title('Revolution')
        track.set_length(326)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('9')
        track.set_title('She Sells Sanctuary')
        track.set_length(263)
        disc.append_track(track)

        track = disc.create_track()
        track.set_number('10')
        track.set_title('Black Angel')
        track.set_length(322)
        disc.append_track(track)

        expected.append_disc(disc)

        s = itunes.ReleaseScraper.from_string('http://music.apple.com/fr/album/love/1028833323')
        r = s.get_result()

        self.assertEqual(expected, r)

    def test_404(self):
        expected = NotFoundResult()
        expected.set_scraper_name(None)

        s = itunes.ReleaseScraper.from_string('http://music.apple.com/us/album/blubb/id999999999999')
        r = s.get_result()

        self.assertEqual(expected, r)

    def test_non_us_404(self):
        expected = NotFoundResult()
        expected.set_scraper_name(None)

        s = itunes.ReleaseScraper.from_string('http://music.apple.com/fr/album/blubb/id999999999999')
        r = s.get_result()

        self.assertEqual(expected, r)

    def test_search_scraper(self):
        s = itunes.SearchScraper('love')
        r = s.get_result()

        self.assertTrue(len(r.get_items()) > 0)
