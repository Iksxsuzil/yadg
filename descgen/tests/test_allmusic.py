#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 tycho
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from . import TestCase
from ..scraper import allmusic
from ..result import ReleaseResult, ListResult, NotFoundResult


class AllmusicTest(TestCase):

    def test_simple_album_tracklist_from_album(self):
        expected = {
            u'artists': u'Aphex Twin',
            u'title': u'Selected Ambient Works 85-92',
            u'format': u'CD',
            u'genres': [u'Electronic'],
            u'styles': [u'Ambient Techno', u'Experimental Techno', u'IDM',
                        u'Techno', u'Experimental Ambient', u'Club/Dance'],
            u'url': u'https://www.allmusic.com/album/release/selected-ambient-works-85-92-mr0001139072',
            u'labelIds': [{u'catalogueNrs': [u'32332'], u'label': u'Apollo Music'}],
            u'releaseEvents': u'1993',
            u'discs': [
                [
                    [u'Xtal', 294],
                    [u'Tha', 547],
                    [u'Pulsewidth', 228],
                    [u'Ageispolis', 323],
                    [u'i', 77],
                    [u'Green Calx', 365],
                    [u'Heliosphan', 293],
                    [u'We Are the Music Makers', 463],
                    [u'Schottkey 7th Path', 308],
                    [u'Ptolemy', 434],
                    [u'Hedphelym', 363],
                    [u'Delphium', 329],
                    [u'Actium', 454],
                ]
            ]
        }

        scraper = allmusic.ReleaseScraper.from_string('https://www.allmusic.com/album/release/selected-ambient-works-85-92-mr0001139072')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_simple_album_tracklist_from_release(self):
        expected = {
            u'artists': u'Aphex Twin',
            u'title': u'Selected Ambient Works 85-92',
            u'format': u'CD',
            u'genres': [u'Electronic'],
            u'styles': [u'Ambient Techno', u'Experimental Techno', u'IDM',
                        u'Techno', u'Experimental Ambient', u'Club/Dance'],
            u'url': u'https://www.allmusic.com/album/release/selected-ambient-works-85-92-mr0004342035',
            u'labelIds': [{u'catalogueNrs': [u'AMB9322CD'], u'label': u'Apollo Records'}],
            u'releaseEvents': u'January 13, 2015',
            u'discs': [
                [
                    [u'Xtal', 294],
                    [u'Tha', 547],
                    [u'Pulsewidth', 228],
                    [u'Ageispolis', 323],
                    [u'i', 77],
                    [u'Green Calx', 365],
                    [u'Heliosphan', 293],
                    [u'We Are the Music Makers', 463],
                    [u'Schottkey 7th Path', 308],
                    [u'Ptolemy', 434],
                    [u'Hedphelym', 363],
                    [u'Delphium', 329],
                    [u'Actium', 454],
                ]
            ]
        }

        scraper = allmusic.ReleaseScraper.from_string('https://www.allmusic.com/album/release/selected-ambient-works-85-92-mr0004342035')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_multiple_cds(self):
        expected = {
            u'artists': u'Talking Heads',
            u'title': u'Once in a Lifetime',
            u'format': u'CD',
            u'genres': [u'Pop/Rock'],
            u'styles': [u'Album Rock', u'Alternative/Indie Rock',
                        u'American Punk', u'College Rock',
                        u'Contemporary Pop/Rock', u'New Wave',
                        u'New York Punk', u'Post-Punk', u'Punk',
                        u'Dance-Rock', u'Alternative Pop/Rock',
                        u'Punk/New Wave'],
            u'url': u'https://www.allmusic.com/album/release/once-in-a-lifetime-mr0001287130',
            u'labelIds': [{u'catalogueNrs': [u'73934'], u'label': u'Rhino'},
                          {u'catalogueNrs': [u'73934'], u'label': u'Sire'},
                          {u'catalogueNrs': [u'73934'], u'label': u'Warner Reprise Video'}],
            u'releaseEvents': u'November 11, 2003',
            u'discs': [
                [
                    [u'Sugar on My Tongue', 155],
                    [u'Love --> Building on Fire', 179],
                    [u'I Wish You Wouldn\'t Say That', 157],
                    [u'Don\'t Worry About the Government', 180],
                    [u'Uh-Oh, Love Comes to Town', 175],
                    [u'New Feeling', 183],
                    [u'Pulled Up', 270],
                    [u'Psycho Killer', 260],
                    [u'Warning Sign', 235],
                    [u'Artists Only', 215],
                    [u'Tentative Decisions', 187],
                    [u'No Compassion', 289],
                    [u'Stay Hungry', 161],
                    [u'I\'m Not in Love', 274],
                    [u'The Book I Read', 249],
                    [u'Thank You for Sending Me an Angel', 133],
                    [u'Found a Job', 301],
                    [u'A Clean Break', 297],
                    [u'Take Me to the River', 304],
                    [u'The Big Country', 331],
                    [u'Heaven', 241],
                ],
                [
                    [u'I Zimbra', 187],
                    [u'Cities', 327],
                    [u'Life During Wartime', 221],
                    [u'Air', 212],
                    [u'Memories Can\'t Wait', 210],
                    [u'Drugs', 213],
                    [u'Once in a Lifetime', 260],
                    [u'Born Under Punches (The Heat Goes On)', 347],
                    [u'Listening Wind', 281],
                    [u'Houses in Motion', 271],
                    [u'Crosseyed and Painless', 285],
                    [u'Burning Down the House', 241],
                    [u'Making Flippy Floppy', 354],
                    [u'Girlfriend Is Better', 343],
                    [u'Slippery People', 305],
                    [u'Swamp', 310],
                    [u'This Must Be the Place (Naive Melody)', 296],
                ],
                [
                    [u'And She Was', 219],
                    [u'Stay Up Late', 224],
                    [u'Creatures of Love', 255],
                    [u'The Lady Don\'t Mind', 244],
                    [u'Road to Nowhere', 260],
                    [u'Wild Wild Life', 221],
                    [u'Love for Sale', 271],
                    [u'People Like Us', 269],
                    [u'Puzzlin\' Evidence', 323],
                    [u'City of Dreams', 309],
                    [u'Blind', 300],
                    [u'Mr. Jones', 261],
                    [u'The Democratic Circus', 304],
                    [u'(Nothing But) Flowers', 333],
                    [u'In Asking Land', 238],
                    [u'Sax and Violins', 318],
                    [u'Lifetime Piling Up', 233],
                ],
                [
                    u'Once in a Lifetime',
                    u'Wild Wild Life',
                    u'Stay Up Late',
                    u'Blind',
                    u'Crosseyed and Painless',
                    u'Burning Down the House',
                    u'And She Was',
                    u'Sax and Violins',
                    u'This Must Be the Place (Naive Melody)',
                    u'The Lady Don\'t Mind',
                    u'Love for Sale',
                    u'Nothing But) Flowers',
                    u'Road to Nowhere'
                ]
            ]
        }

        scraper = allmusic.ReleaseScraper.from_string(u'https://www.allmusic.com/album/release/once-in-a-lifetime-mr0001287130')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_featuring_track_artist(self):
        expected = {
            u'artists': u'OutKast',
            u'title': u'Aquemini',
            u'format': u'CD',
            u'genres': [u'Rap'],
            u'styles': [u'Alternative Rap', u'Dirty South', u'Southern Rap'],
            u'url': u'https://www.allmusic.com/album/release/aquemini-mr0001128740',
            u'labelIds': [{u'catalogueNrs': [u'73008260532'], u'label': u'LaFace'}],
            u'releaseEvents': u'September 29, 1998',
            u'discs': [
                [
                    [u'Hold on, Be Strong', 71],
                    [u'Return of the "G"', 289],
                    [u'Rosa Parks', 324],
                    [
                        [{u'name': u'Raekwon', u'types': [u'guest']}],
                        u'Skew It on the Bar-B', 195
                    ],
                    [u'Aquemini', 319],
                    [
                        [{u'name': u'George Clinton', u'types': [u'guest']}],
                        u'Synthesizer', 311
                    ],
                    [u'Slump', 309],
                    [u'West Savannah', 243],
                    [u'Da Art of Storytellin\', Pt. 1', 223],
                    [u'Da Art of Storytellin\', Pt. 2', 168],
                    [u'Mamacita', 352],
                    [u'Spottieottiedopaliscious', 427],
                    [
                        [
                            {u'name': u'Big Gipp', u'types': [u'guest']},
                            {u'name': u'Khujo', u'types': [u'guest']},
                            {u'name': u'T-Mo', u'types': [u'guest']}
                        ],
                        u'Y\'All Scared', 290
                    ],
                    [u'Nathaniel', 70],
                    [
                        [{u'name': u'C-Lo', u'types': [u'guest']}],
                        u'Liberation', 526
                    ],
                    [u'Chonkyfire', 370],
                ]
            ]
        }

        scraper = allmusic.ReleaseScraper.from_string(u'https://www.allmusic.com/album/release/aquemini-mr0001128740')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_various_artists(self):
        expected = {
            u'artists': [{u'name': None, u'isVarious': True}],
            u'title': u'Gothic File, Vol. 4',
            u'format': u'CD',
            u'genres': [u'Pop/Rock'],
            u'url': u'https://www.allmusic.com/album/release/gothic-file-vol-4-mr0001995330',
            u'labelIds': [{u'catalogueNrs': [], u'label': u'Gothic Records'}],
            u'releaseEvents': u'2008',
            u'discs': [
                [
                    [u'Schandmaul', u'Königin', 244],
                    [u'Jesus on Extasy', u'Beloved Enemy', 268],
                    [u'Reflexion', u'Twilight Child', 201],
                    [u'Edenbridge', u'Paramount', 264],
                    [u'Tiamat', u'Equinox of the Gods', 274],
                    [u'N.F.D.', u'The Unforgiven', 201],
                    [u'Bauhaus', u'Adrenalin', 340],
                    [u'IAMX', u'Nightlife', 300],
                    [[u'Marc Almond', u'Punx Soundcheck'], u'Saint Now', 346],
                    [u'Psyche', u'Disorder', 231],
                    [u'Die Perlen', u'Beweg Dich', 244],
                    [u'HorrorPops', u'Heading for the Disco?', 167],
                    [u'The Other', u'Freak Liberation Front', 206],
                    [u'Uebermutter', u'Unheil!', 218],
                    [u'X-Fusion', u'When the Curtain Falls', 285],
                    [u'Shnarph!', u'Der Eiertanz', 248],
                    [u'Straftanz', u'Tanzt Kaputt, Was Euch Kaputt Macht', 289],
                    [u':wumpscut:', u'Schäbiger Lump', 257],
                ]
            ]
        }

        scraper = allmusic.ReleaseScraper.from_string(u'https://www.allmusic.com/album/release/gothic-file-vol-4-mr0001995330')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_release_without_artist(self):
        expected = {
            u'artists': u'Unknown Artist',
            u'title': u'Sartorio: L\'Orfeo',
            u'format': u'CD',
            u'genres': [u'Classical'],
            u'styles': [u'Opera'],
            u'url': u'https://www.allmusic.com/album/release/sartorio-lorfeo-mr0002117152',
            u'labelIds': [{u'catalogueNrs': [u'96'], u'label': u'Fonit-Cetra Italia'}],
            u'discs': []
        }

        scraper = allmusic.ReleaseScraper.from_string(u'https://www.allmusic.com/album/release/sartorio-lorfeo-mr0002117152')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_release_empty_title_tracks(self):
        expected = {
            u'artists': u'Aphex Twin',
            u'title': u'Selected Ambient Works, Vol. 2 [LP]',
            u'format': u'LP',
            u'genres': [u'Electronic'],
            u'styles': [u'Ambient Techno', u'Experimental Ambient', u'Experimental Electronic',
                        u'Experimental Techno', u'Techno', u'IDM', u'Club/Dance'],
            u'url': u'https://www.allmusic.com/album/release/selected-ambient-works-vol-2-lp-mr0003557697',
            u'labelIds': [{u'catalogueNrs': [u'IF 11TLP'], u'label': u'1972'}],
            u'releaseEvents': u'March 19, 2012',
            u'discs': [
                [{u'title': '[missing title information]'}],
                [{u'title': '[missing title information]'}],
                [{u'title': '[missing title information]'}]
            ]
        }

        scraper = allmusic.ReleaseScraper.from_string(u'https://www.allmusic.com/album/release/selected-ambient-works-vol-2-lp-mr0003557697')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_release_without_tracks(self):
        expected = {
            u'artists': u'Ayumi Hamasaki',
            u'title': u'Love',
            u'format': u'CD',
            u'genres': [u'Vocal'],
            u'url': u'https://www.allmusic.com/album/release/love-mr0004151701',
            u'labelIds': [{u'catalogueNrs': [u'1074591'], u'label': u'AVEX'}],
            u'releaseEvents': u'November 13, 2012',
            u'discs': []
        }

        scraper = allmusic.ReleaseScraper.from_string(u'https://www.allmusic.com/album/release/love-mr0004151701')
        result = scraper.get_result()

        self.assertExpectedReleaseResult(expected, result)

    def test_master_release(self):
        expected = [
            [
                u'Iron Maiden – Piece of Mind',
                u'1987 | Capitol C2-46363 | CD',
                u'https://www.allmusic.com/album/release/piece-of-mind-mr0001409490'
            ],
            [
                u'Iron Maiden – Piece of Mind',
                u'1998 | Raw Power Records 130 | CD',
                u'https://www.allmusic.com/album/release/piece-of-mind-mr0001286595'
            ],
            [
                u'Iron Maiden – Piece of Mind',
                u'1998 | EMI Music Distribution 4969190 | CD',
                u'https://www.allmusic.com/album/release/piece-of-mind-mr0002762625'
            ],
            [
                u'Iron Maiden – Piece of Mind',
                u'1998 | EMI Digital / Parlophone / Warner Music 0724349691957 | Digital',
                u'https://www.allmusic.com/album/release/piece-of-mind-mr0003199545'
            ],
            [
                u'Iron Maiden – Piece of Mind',
                u'1998 | BMG Rights Management / Iron Maiden Holdings / Sanctuary / Warner Music 2778198DIG | Digital',
                u'https://www.allmusic.com/album/release/piece-of-mind-mr0003437731'
            ],
            [
                u'Iron Maiden – Piece of Mind [Limited Edition]',
                u'2002 | Sony Music Distribution 86045 | CD',
                u'https://www.allmusic.com/album/release/piece-of-mind-limited-edition-mr0001286696'
            ],
            [
                u'Iron Maiden – Piece of Mind [Limited Edition]',
                u'2002 | Metal-Is 86211 | CD',
                u'https://www.allmusic.com/album/release/piece-of-mind-limited-edition-mr0000011844'
            ],
            [
                u'Iron Maiden – Piece of Mind',
                u'2008 | Toshiba EMI 53759 | CD',
                u'https://www.allmusic.com/album/release/piece-of-mind-mr0001284871'
            ],
            [
                u'Iron Maiden – Piece of Mind',
                u'2012 | EMI 972 9491 | LP',
                u'https://www.allmusic.com/album/release/piece-of-mind-mr0003773758'
            ],
            [
                u'Iron Maiden – Piece of Mind',
                u'2012 | Universal B 001755901 | LP',
                u'https://www.allmusic.com/album/release/piece-of-mind-mr0003803759'
            ],
            [
                u'Iron Maiden – Piece of Mind [LP]',
                u'2014 | BMG / Sanctuary BMG 14013 | LP',
                u'https://www.allmusic.com/album/release/piece-of-mind-lp-mr0004273694'
            ],
            [
                u'Iron Maiden – Piece of Mind [Remastered]',
                u'2014 | WEA Japan 7416337 | CD',
                u'https://www.allmusic.com/album/release/piece-of-mind-remastered-mr0004066894'
            ]
        ]

        scraper = allmusic.AlbumScraper.from_string('https://www.allmusic.com/album/piece-of-mind-mw0000045853')
        result = scraper.get_result()

        self.assertExpectedListResult(expected, result)

    def test_not_fount(self):
        expected = NotFoundResult()
        expected.set_scraper_name(None)

        scraper = allmusic.ReleaseScraper.from_string(u'https://www.allmusic.com/album/release/selected-ambient-works-85-92-mr9999999999')
        result = scraper.get_result()

        self.assertEqual(expected, result)

    def test_search_scraper(self):
        scraper = allmusic.SearchScraper('lurve')
        result = scraper.get_result()

        self.assertTrue(len(result.get_items()) > 0)
