#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from django.test import TestCase as TestCaseBase
from ..result import ReleaseResult, ListResult, NotFoundResult, Result
from ..visitor.misc import JSONSerializeVisitor


def todict(obj):
    if hasattr(obj, "__iter__"):
        return [todict(v) for v in obj]
    elif hasattr(obj, "__dict__"):
        return dict([(key, todict(value))
                     for key, value in obj.__dict__.iteritems()
                     if not callable(value) and not key.startswith('_')])
    else:
        return obj


def merge_dict(first, *rests):
    result = first.copy()
    for dic in rests:
        result.update(dic)
    return result


class TestCase(TestCaseBase):

    maxDiff = None

    def setUp(self):
        self.addTypeEqualityFunc(ReleaseResult, 'assertResultEqual')
        self.addTypeEqualityFunc(ListResult, 'assertResultEqual')
        self.addTypeEqualityFunc(NotFoundResult, 'assertResultEqual')

    def assertResultEqual(self, d1, d2, msg=None):
        self.assertTrue(issubclass(d1.__class__, Result), 'First argument is not a Result')
        self.assertTrue(issubclass(d2.__class__, Result), 'Second argument is not a Result')

        self.assertEqual(d1.__class__.__name__, d2.__class__.__name__)
        self.assertEqual(todict(d1), todict(d2), msg)

    def assertExpectedReleaseResult(self, expected, result, msg=None):
        self.assertTrue(isinstance(expected, dict), 'First argument is not a dict')
        self.assertTrue(issubclass(result.__class__, ReleaseResult), 'Second argument is not a ReleaseResult')

        expected_with_default = self._default_expected_release(expected)
        jsv = JSONSerializeVisitor()

        self.assertEqual(jsv.visit(result), expected_with_default, msg)

    def assertExpectedListResult(self, expected, result, msg=None):
        self.assertTrue(isinstance(expected, list), 'First argument is not a list')
        self.assertTrue(issubclass(result.__class__, ListResult), 'Second argument is not a ListResult')

        expected_with_default = self._default_expected_list(expected)
        jsv = JSONSerializeVisitor()

        self.assertEqual(jsv.visit(result), expected_with_default, msg)

    def _default_expected_release(self, expected):
        result = {
            u'albumArts': [],
            u'format': None,
            u'genres': [],
            u'labelIds': [],
            u'releaseEvents': [],
            u'styles': [],
            u'type': u'ReleaseResult'
        }

        result.update(expected)

        result[u'artists'] = self._default_artists(result[u'artists'])
        result[u'discs'] = self._default_discs(result[u'discs'])
        result[u'releaseEvents'] = self._default_release_events(result[u'releaseEvents'])

        return result

    def _default_expected_list(self, expected):
        if isinstance(expected, list):
            return {
                u'items': [self._default_item(item) for item in expected],
                u'type': u'ListResult'
            }

        return expected

    def _default_item(self, item):
        item_keys = [u'name', u'info', u'url', u'query']
        default_item = dict.fromkeys(item_keys, u'')
        default_item[u'query_scraper'] = None

        if isinstance(item, list):
            # item of length 2 have name and url, insert empty info
            if len(item) == 2:
                item.insert(1, u'')

            # item of length 3 have name, info and url, repeat url as query
            if len(item) == 3:
                item.append(item[2])

            item = dict(zip(item_keys, item))

        return merge_dict(default_item, item)

    def _default_artist(self, artist):
        default_artist = {
            u'isVarious': False,
            u'types': [u'main']
        }

        if isinstance(artist, unicode):
            artist = {u'name': artist}

        return merge_dict(default_artist, artist)

    def _default_artists(self, artists):
        if isinstance(artists, unicode):
            artists = [artists]

        return [self._default_artist(artist) for artist in artists]

    def _default_track(self, track):
        default_track = {
            u'artists': [],
            u'length': None
        }

        if isinstance(track, unicode):
            track = {
                u'title': track
            }

        if isinstance(track, list):
            number = None
            artists = []

            if len(track) == 4:
                number = track.pop(0)

            if len(track) == 3:
                artists = track.pop(0)

            track = {
                u'title': track[0],
                u'length': track[1],
                u'artists': artists
            }

            if number:
                track[u'number'] = number

        result = merge_dict(default_track, track)
        result[u'artists'] = self._default_artists(result[u'artists'])

        return result

    def _default_tracks(self, tracks):
        return [merge_dict({u'number': unicode(index + 1)}, self._default_track(track))
                for index, track in enumerate(tracks)]

    def _default_disc(self, disc):
        default_disc = {
            u'title': None
        }

        if isinstance(disc, list):
            disc = {
                u'tracks': disc
            }

        disc[u'tracks'] = self._default_tracks(disc[u'tracks'])
        return merge_dict(default_disc, disc)

    def _default_discs(self, discs):
        return [merge_dict({u'number': index + 1}, self._default_disc(disc))
                for index, disc in enumerate(discs)]

    def _default_release_events(self, release_event):
        if isinstance(release_event, unicode):
            release_event = [{
                u'country': None,
                u'date': release_event
            }]

        return release_event
