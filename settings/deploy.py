#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011-2015 Slack
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
from .base import *


DEBUG = False
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': secret.DATABASE_NAME,                      # Or path to database file if using sqlite3.
        'USER': secret.DATABASE_USER,                      # Not used with sqlite3.
        'PASSWORD': secret.DATABASE_PASSWORD,                  # Not used with sqlite3.
        'HOST': secret.DATABASE_HOST,                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': secret.DATABASE_PORT,                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Set log level for scraper logger
LOGGING['handlers']['scraper_console']['level'] = 'WARNING'
LOGGING['loggers']['descgen.scraper']['level'] = 'WARNING'

# deployment specific Celery settings
CELERYD_CONCURRENCY = 8

#set allowed hosts
ALLOWED_HOSTS = secret.ALLOWED_HOSTS
