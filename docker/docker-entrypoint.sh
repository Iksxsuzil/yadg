#!/usr/bin/env bash
set -e

USER=${USER:-yadg}
SECRETS_FILE=secret.py
PUBLIC_STATIC=public_static
DO_MIGRATION=${DO_MIGRATION:-n}


create_secrets_file () {
cat <<EOF > $1
#!/usr/bin/env python
# -*- coding: utf-8 -*-
EOF
}


if [ ! -f "${SECRETS_FILE}" ]; then
    create_secrets_file ${SECRETS_FILE}

    # take all env variables beginning with SECRET_ and write them to the secrets file
    while IFS='=' read -r name value ; do
        if [[ $name == 'SECRET_'* ]]; then
            var_name=${name#SECRET_} # delete 'SECRET_' from beginning of var
            echo "${var_name} = ${value}" >> ${SECRETS_FILE}
        fi
    done < <(env)
fi

if [ "$1" = "gunicorn" ]; then
    # this will be executed as root because we don't want to make assumptions about the permissions of the folder
    python manage.py collectstatic --noinput

    # migrate the database if desired
    if [ "$DO_MIGRATION" = "y" ]; then
        gosu "${USER}" python manage.py migrate --noinput
    fi

    exec gosu "${USER}" "$@"
fi

exec "$@"
