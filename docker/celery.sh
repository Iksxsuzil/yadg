#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

source "${DIR}/redis-bash-lib"

USER=${USER:-yadg}
REDIS_PORT=${REDIS_PORT:-6379}
REDIS_KEY=${REDIS_KEY:-celerynode}
NODE_ID_FILE=node_id
NODE_ID=$(cat "${NODE_ID_FILE}" 2>/dev/null)

if [  -z "${NODE_ID}" ]; then
    # we have no node id yet so we need to get one from redis
    if [ ! -z "${REDIS_HOST}" ] && [ ! -z "${REDIS_PORT}" ]; then
        if exec 6<>"/dev/tcp/${REDIS_HOST}/${REDIS_PORT}"; then # open the connection
            NODE_ID=$(redis-client 6 INCR "$REDIS_KEY") # get a monotone increasing node id
            exec 6>&- # close the connection

            if [ -z "${NODE_ID}" ]; then
                echo "Could not get node id"
                exit 1
            fi

            echo "${NODE_ID}" > "${NODE_ID_FILE}"
        fi
    else
        echo "No REDIS_HOST or REDIS_PORT given"
        exit 1
    fi
fi

START_COMMAND=$(gosu "${USER}" python manage.py celerycmdline start -n "${NODE_ID}")
RATE_LIMIT_COMMAND=$(gosu "${USER}" python manage.py celerycmdline rate_limit -n "${NODE_ID}")

if [ ! -z "${START_COMMAND}" ]; then
    if [ ! -z "${RATE_LIMIT_COMMAND}" ]; then
        echo "Scheduling rate limiting command"
        (sleep 15 && echo "Executing rate limiting command" && eval gosu "${USER}" "${RATE_LIMIT_COMMAND}") &
    fi
    echo "Executing start command"
    eval "exec gosu "${USER}" ${START_COMMAND}"
else
    echo "Got no start command for node id ${NODE_ID}"
    exit 1
fi
